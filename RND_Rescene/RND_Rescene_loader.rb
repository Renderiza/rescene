module RND_Extensions
	#
	module RND_Rescene
		#
		class Rescene
			#
			@@model = Sketchup.active_model
			@@path = File.dirname(__FILE__) # this plugin's folder
			@@rnd_rescene = "rnd_rescene.html"
			@@rescene_files = File.join(@@path, '/ResceneFiles/')
			@@rescene_dlg = nil
			#
			def initialize()
				#
				@rescene_1_file =  File.join(@@path, @@rnd_rescene)
				#
				if (!@rescene_1_file) then
					UI.messagebox("Cannot find file '#{@@rnd_rescene} in folder '#{@@path}'")
					return
				end
				#
				if @@rescene_dlg == nil
					@@rescene_dlg = UI::WebDialog.new("[RE]SCENE v.1.0.7", false, "[RE]SCENE v.1.0.7", 280, 675, 70, 95, true)
					@@rescene_dlg.add_action_callback("push_frame") do |d,p|
					push_frame(d,p)
					end
				end
				#
				@@rescene_dlg.set_size(280, 675)
				@@rescene_dlg.set_file(@rescene_1_file)
				@@rescene_dlg.show()
				#
			end #def



			def push_frame(dialog,data)
				#
				params = query_to_hash(data) # parse state information
				#
				if params['all_single'].to_s  == "all"
				@all_single = 1
				else
				@all_single = 0
				end
				#
				# Text Value
				@text_value = params['getsearch'].to_s
				#
				#Click [Re]Order
				if params['reorder'].to_s == "1"
					model = Sketchup.active_model
					pages = model.pages
					num = pages.count
					#
					if num > 0
						selpage = pages.selected_page.name
						c_page = pages.selected_page
						opages = pages.map{|pg| pg.name }.sort!
						num = opages.length
						#
						opages.each{ |page|
							optt = pages[page].transition_time
							pages[page].transition_time = 0
							opdt = pages[page].delay_time
							pages[page].delay_time = 0
							pages.selected_page = pages[page]
							newp = pages.add(page)
							newp.transition_time = optt
							newp.delay_time = opdt
						}
						#
						num.times{ pages.erase(model.pages[0]) }
						pages.selected_page = pages[selpage]
					end
					#
					reorder = 0
					script = "top.reorder = " + reorder.to_s + ";"
					dialog.execute_script(script)
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
					#
				end
				#
				#Click [Re]Name
				if params['rename'].to_s == "1"
					model = Sketchup.active_model
					pages = model.pages
					num = pages.count
					#
					if num > 0
						@page_name = "#{@text_value}"
						@page_num = 1
						selpage = pages.selected_page.name
						#
						if @all_single == 1
							pages.each do |page|
								if page.label[-1..-1] != ")"
									page.name = "#{@page_name}#{@page_num}"
									@page_num = @page_num + 1
								end
							end
						else
							pages.selected_page = pages[selpage]
							pages[selpage].name = "#{@page_name}"
						end
					end
					#
					rename = 0
					script = "top.rename = " + rename.to_s + ";"
					dialog.execute_script(script)
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
				end
				#
				if params['update_camera'].to_s  == "true"
				@update_camera = 1
				else
				@update_camera = 0
				end
				#
				if params['update_style'].to_s == "true"
				@update_style = 2
				else
				@update_style = 0
				end
				#
				if params['update_shadow'].to_s == "true"
				@update_shadow = 4
				else
				@update_shadow = 0
				end
				#
				if params['update_axes'].to_s == "true"
				@update_axes = 8
				else
				@update_axes = 0
				end
				#
				if params['update_hidden'].to_s == "true"
				@update_hidden = 16
				else
				@update_hidden = 0
				end
				#
				if params['update_layers'].to_s == "true"
				@update_layers = 32
				else
				@update_layers = 0
				end
				#
				if params['update_section'].to_s == "true"
				@update_section = 64
				else
				@update_section = 0
				end
				#
				#Click Update
				if params['active'].to_s == "1"
					#
					Sketchup.active_model.select_tool RND_Extensions::RND_Rescene::Rescene.new
					#
					#########
					###RUN###
					#########
					#
					model = Sketchup.active_model
					pages = model.pages
					layers = model.layers
					c_page = pages.selected_page
					#
					if @all_single == 1
						pages.each {|page|
						#
						if page.label[-1..-1] != ")"
						#
						###########
						page.update(@update_camera + @update_style + @update_shadow + @update_axes + @update_hidden + @update_layers + @update_section)
						###########
						#
						end
						#
						} #Pages.each
					else
						if c_page.label[-1..-1] != ")"
						#
						###########
						c_page.update(@update_camera + @update_style + @update_shadow + @update_axes + @update_hidden + @update_layers + @update_section)
						###########
						#
						end
						#
					end
					#
					activate = 0
					script = "top.active = " + activate.to_s + ";"
					dialog.execute_script(script)
					#
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
					#
				end #Done
				#
				#Click Done
				if params['done'].to_s == "1"
					Sketchup.send_action('selectSelectionTool:') #Selection Tool
					@@rescene_dlg.close
				end
				#
				#Click Previous
				if params['previous'].to_s == "1"
					Sketchup.send_action('pagePrevious:')
					prev = 0
					script = "top.previous = " + prev.to_s + ";"
					dialog.execute_script(script)
				end
				#
				#Click Next
				if params['next'].to_s == "1"
					Sketchup.send_action('pageNext:')
					nxt = 0
					script = "top.next = " + nxt.to_s + ";"
					dialog.execute_script(script)
				end
				#
				#Click Delete
				if params['remove'].to_s == "1"
					Sketchup.send_action('pageDelete:')
					del = 0
					script = "top.remove = " + del.to_s + ";"
					dialog.execute_script(script)
				end
				#
				#Click Add
				if params['add'].to_s == "1"
					Sketchup.send_action('pageAdd:')
					addin = 0
					script = "top.add = " + addin.to_s + ";"
					dialog.execute_script(script)
				end
				#
				dialog.set_on_close{ Sketchup.send_action('selectSelectionTool:') }
				#
			end #def



			def unescape(string)
				if string != nil
					string = string.gsub(/\+/, ' ').gsub(/((?:%[0-9a-fA-F]{2})+)/n) do
						[$1.delete('%')].pack('H*')
					end
				end
				return string
			end #def



			def query_to_hash(query)
				param_pairs = query.split('&')
				param_hash = {}
				for param in param_pairs
					name, value = param.split('=')
					name = unescape(name)
					value = unescape(value)
					param_hash[name] = value
				end
				return param_hash
			end #def
			#
		end #class
		#
	end #module
	#
end #module
#
file_loaded( __FILE__ )
