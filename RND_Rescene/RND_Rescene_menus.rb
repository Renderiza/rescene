module RND_Extensions
	#
	module RND_RESCENE
		require 'sketchup.rb'
		require 'RND_Rescene/RND_Rescene_loader.rb'
	end
	#
	if !file_loaded?('rnd_menu_loader')
		@@rnd_tools_menu = UI.menu("Plugins").add_submenu("Renderiza Tools")
	end
	#
	#------New menu Items---------------------------
	if !file_loaded?('rnd_ew_loader')
		@@rnd_ew_menu = @@rnd_tools_menu.add_submenu("Websites")
		@@rnd_ew_menu.add_separator
		@@rnd_ew_menu.add_item("PluginStore"){UI.openURL('http://sketchucation.com/resources/pluginstore?pauthor=renderiza')}
		@@rnd_ew_menu.add_item("Extension Warehouse"){UI.openURL('http://extensions.sketchup.com/en/user/5451/store')}
		@@rnd_tools_menu.add_separator
		@@rnd_ew_menu.add_separator
	end
	#------------------------------------------------
	#
	if !file_loaded?(__FILE__) #then
		@@rnd_tools_menu.add_item('[Re]Scene') {
			Sketchup.active_model.select_tool RND_Extensions::RND_Rescene::Rescene.new
		}
		# Add toolbars
		rnd_rescene_tb = UI::Toolbar.new "[Re]Scene"
		rnd_rescene_cmd = UI::Command.new("[Re]Scene") {
			Sketchup.active_model.select_tool RND_Extensions::RND_Rescene::Rescene.new
		}
		rnd_rescene_redo_cmd = UI::Command.new("[Redo]Scene") {
			model = Sketchup.active_model
			pages = model.pages
			layers = model.layers
			c_page = pages.selected_page
			#
			pages.each {|page|
				if page.label[-1..-1] != ")"
					###########
					page.update(2)
					###########
				end
			}
		}
		#
		# icons toolbar
		rnd_rescene_cmd.small_icon = "img/cad_rescene_1_16.png"
		rnd_rescene_cmd.large_icon = "img/cad_rescene_1_24.png"
		rnd_rescene_cmd.tooltip = "[Re]Scene"
		rnd_rescene_cmd.status_bar_text = "Set current page style to others pages."
		rnd_rescene_cmd.menu_text = "[Re]Scene"
		rnd_rescene_tb = rnd_rescene_tb.add_item rnd_rescene_cmd
		rnd_rescene_tb.show
		#
		# icons toolbar redo
		rnd_rescene_redo_cmd.small_icon = "img/cad_rescene_2_16.png"
		rnd_rescene_redo_cmd.large_icon = "img/cad_rescene_2_24.png"
		rnd_rescene_redo_cmd.tooltip = "[Redo]Scene"
		rnd_rescene_redo_cmd.status_bar_text = "Set current page style to others pages without web-dialog."
		rnd_rescene_redo_cmd.menu_text = "[Redo]Scene"
		rnd_rescene_tb = rnd_rescene_tb.add_item rnd_rescene_redo_cmd
		rnd_rescene_tb.show
		#
		# Add Context Menu
		UI.add_context_menu_handler{|menu|
			rclick_menu  = menu.add_submenu("[Re]Scene")
			rclick_menu.add_item('Open'){
				Sketchup.active_model.select_tool RND_Extensions::RND_Rescene::Rescene.new
				Sketchup.send_action('selectSelectionTool:')
			}
			#
			rclick_menu.add_item('Update'){
				model = Sketchup.active_model
				pages = model.pages
				layers = model.layers
				c_page = pages.selected_page
				#
				pages.each {|page|
					if page.label[-1..-1] != ")"
						###########
						page.update(2)
						###########					
					end
				}
			}
			#
			rclick_menu.add_separator
			#
			rclick_menu.add_item('Previous Scene'){
				Sketchup.send_action('pagePrevious:')
			}
			#
			rclick_menu.add_item('Next Scene'){
				Sketchup.send_action('pageNext:')
			}
			#
			rclick_menu.add_item('Delete Scene'){
				Sketchup.send_action('pageDelete:')
			}
			#
			rclick_menu.add_item('Add Scene'){
				Sketchup.send_action('pageAdd:')
			}
		}
		#
	end
	#
	file_loaded('rnd_ew_loader')
	file_loaded('rnd_menu_loader')
	file_loaded(__FILE__)
end
