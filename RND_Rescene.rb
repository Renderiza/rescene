=begin
(c) CadFather 2013
(c) Renderiza 2013

Permission to use, copy, modify, and distribute this software for 
any purpose and without fee is hereby granted, provided the above
copyright notice appear in all copies.

THIS SOFTWARE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

== Information
Authors:: 
	*CadFather
	*Renderiza
Name:: [RE]SCENE
Version:: 1.0.8
SU Version:: v8
Date:: 9/8/2013
Description:: Applies current page style to others pages that are included in animation with just one click. 
=end

module RND_Extensions
	#
	module RND_Rescene
		require 'sketchup.rb'
		require 'extensions.rb'
		#
		unless file_loaded?( __FILE__ )
			file_loaded( __FILE__ )
			# Create the extension.
			ext = SketchupExtension.new '[Re]Scene', 'RND_Rescene/RND_Rescene_menus.rb'
			#
			# Attach some nice info.
			ext.creator     = '| Renderiza | CadFather |'
			ext.version     = '1.0.8'
			ext.copyright   = '2013, Renderiza Studio.'
			ext.description = 'Applies current page style to others pages that are included in animation.'
			#
			# Register and load the extension on startup.
			Sketchup.register_extension ext, true
		end
		#
	end
	#
end
#
file_loaded( __FILE__ )
